<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 9/29/17
 * Time: 12:49 PM
 */

class Eye {
    private $color;

    function __construct(string $color)
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}

class Head {
    private $leftEye;
    private $rightEye;

    function __construct(Eye $leftEye, Eye $rightEye)
    {
        $this->leftEye = $leftEye;
        $this->rightEye = $rightEye;
    }

    /**
     * @return Eye
     */
    public function getLeftEye(): Eye
    {
        return $this->leftEye;
    }

    /**
     * @return Eye
     */
    public function getRightEye(): Eye
    {
        return $this->rightEye;
    }
}

interface Movable {
    function moveUp();
    function moveDown();
    function moveForward();
    function moveBackward();
}

abstract class Arm implements Movable {
    function moveUp() {
        echo get_class($this)." is moved UP\n";
    }

    function moveDown() {
        echo get_class($this)." is moved DOWN\n";
    }

    function moveForward() {
        echo get_class($this)." is moved FORWARD\n";
    }

    function moveBackward() {
        echo get_class($this)." is moved BACKWARD\n";
    }
}

class LeftArm extends Arm{
    private $leght;

    function __construct(int $lenght)
    {
        $this->leght = $lenght;
    }
}



class RightArm extends Arm{
    private $leght;

    function __construct(int $lenght)
    {
        $this->leght = $lenght;
    }
}

abstract class Leg implements Movable {
    function moveUp() {
        echo get_class($this)." is moved UP\n";
    }

    function moveDown() {
        echo get_class($this)." is moved DOWN\n";
    }

    function moveForward() {
        echo get_class($this)." is moved FORWARD\n";
    }

    function moveBackward() {
        echo get_class($this)." is moved BACKWARD\n";
    }
}

class LeftLeg extends Leg{
    private $leght;

    function __construct(int $lenght)
    {
        $this->leght = $lenght;
    }
}



class RightLeg extends Leg{
    private $leght;

    function __construct(int $lenght)
    {
        $this->leght = $lenght;
    }
}

class Body {
    private $head;
    private $leftArm;
    private $rightArm;
    private $leftLeg;
    private $rightLeg;

    function __construct(Head $head, LeftArm $leftArm, RightArm $rightArm, LeftLeg $leftLeg, RightLeg $rightLeg)
    {
        $this->head = $head;
        $this->leftArm = $leftArm;
        $this->rightArm = $rightArm;
        $this->leftLeg = $leftLeg;
        $this->rightLeg = $rightLeg;
    }

    /**
     * @return Head
     */
    public function getHead(): Head
    {
        return $this->head;
    }

    /**
     * @return LeftArm
     */
    public function getLeftArm(): LeftArm
    {
        return $this->leftArm;
    }

    /**
     * @return RightArm
     */
    public function getRightArm(): RightArm
    {
        return $this->rightArm;
    }

    /**
     * @return LeftLeg
     */
    public function getLeftLeg(): LeftLeg
    {
        return $this->leftLeg;
    }

    /**
     * @return RightLeg
     */
    public function getRightLeg(): RightLeg
    {
        return $this->rightLeg;
    }
}

// implementation

$body = new Body(
    new Head(
        new Eye('black'),
        new Eye('black')
    ),
    new LeftArm(60),
    new RightArm(60),
    new LeftLeg(80),
    new RightLeg(80)
);

echo "LeftEye color is: ".$body->getHead()->getLeftEye()->getColor()."\n";
$body->getRightLeg()->moveForward();
$body->getLeftLeg()->moveForward();
$body->getLeftArm()->moveUp();
$body->getRightArm()->moveDown();